/**
 * @param key {string}
 * @param value
 * */


import {isArray} from "../../../utils/helpers";
import {ArgumentsProvider} from "./ArgumentsProvider";
import {ArgumentsHandler} from "./ArgumentsHandler";

export const ArgList = function (key, value) {
    if (typeof key !== "string" || !key || !isArray(value)) return;
    const argProvider = ArgumentsProvider.getInstance();
    argProvider.setHandler(new ArgumentsHandler(null));
    argProvider.handle({key, value, isList: true});
}

