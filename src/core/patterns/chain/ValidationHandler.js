import {Handler} from "./Handler";

// TODO
export class ValidationHandler extends Handler {
    /**@param next {Handler}*/
    constructor(next) {
        super(next);
    }
    /**@override*/
    doHandle(value) {
        console.log("validating value   :::: -> ", value);
        return false;
    }

    getName() {
        return "ValidationHandler";
    }
}
