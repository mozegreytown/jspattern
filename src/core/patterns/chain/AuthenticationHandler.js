import {Handler} from "./Handler";

// TODO
export class AuthenticationHandler extends Handler {
    /**@param next {Handler}*/
    constructor(next) {
        super(next);
    }
    /**@override*/
    doHandle(value) {
        const isValue = value && value === "admin";
        // console.log("handled by authenticator");
        return !isValue;
    }

    getName() {
        return "AuthenticationHandler";
    }
}
