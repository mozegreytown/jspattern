import {Handler} from "./Handler";
import {isObject} from "../../../utils/helpers";

export class DomFieldHandler extends Handler {
    /**@param next {Handler}*/
    constructor(next) {
        super(next);
    }
    /**@override
     * @param option {Object}
     * */
    doHandle(option) {
        if (!isObject(option)) {
            return new Error(`Parameter should be an object but ${typeof option} given`);
        }
        // console.log(`${this.getName()} is set  :::: `,  option.updateDom !== null && option.updateDom !== undefined);
        return !option.updateDom;
    }

    getName() {
        return "DomFieldHandler";
    }
}
