import {Handler} from "./Handler";
import {isObject} from "../../../utils/helpers";

export class ProviderFieldHandler extends Handler {
    /**@param next {Handler}*/
    constructor(next) {
        super(next);
    }
    /**@override
     * @param option {Object}
     * */
    doHandle(option) {
        if (!isObject(option)) {
            return new Error(`Parameter should be an object but ${typeof option} given`);
        }
        // console.log(`${this.getName()} is set  :::: `,  option.useReduxProvider !== null && option.useReduxProvider !== undefined);
        return !option.useReduxProvider;
    }

    getName() {
        return "ProviderFieldHandler";
    }
}
