import {ContainerHandler} from "./";

export class ListItemClass extends ContainerHandler {
    constructor(props) {
        super(props)
    }
    /** @override **/
    getTagName() {
        return "li";
    }
    /** @override **/
    getClassName() {
        return "";
    }
    /** @override **/
    getStyle() {
    }
   
    /** @override **/
    getName(){
        return "ListItemClass"
    }
}
