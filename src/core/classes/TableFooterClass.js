import {Parameters} from "./";

export class TableFooterClass extends Parameters {
    constructor(props) {
        super(props)
    }
    /** @override **/
    getTagName() {
        return "tfoot";
    }
    /** @override **/
    getClassName() {
        return "";
    }
    /** @override **/
    getStyle() {
    }
    /** @override **/
    setObjectList() {
    }

    /** @override **/
    getName(){
        return "TableFooterClass"
    }
}
