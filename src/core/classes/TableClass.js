import {Parameters} from "./";

export class TableClass extends Parameters {
    constructor(props) {
        super(props)
    }
    /** @override **/
    getTagName() {
        return "table";
    }
    /** @override **/
    getClassName() {
        return "";
    }
    /** @override **/
    getStyle() {
    }
    /** @override **/
    setObjectList() {
    }

    /** @override **/
    getName(){
        return "TableClass"
    }
}
