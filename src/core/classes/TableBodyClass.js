import {Parameters} from "./";

export class TableBodyClass extends Parameters {
    constructor(props) {
        super(props)
    }
    /** @override **/
    getTagName() {
        return "tbody";
    }
    /** @override **/
    getClassName() {
        return "";
    }
    /** @override **/
    getStyle() {
    }
    /** @override **/
    setObjectList() {
    }

   
    /** @override **/
    getName(){
        return "TableBodyClass"
    }
}
