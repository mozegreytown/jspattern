import {ContainerHandler} from "./";

export class SpanClass extends ContainerHandler {
    constructor(props) {
        super(props)
    }

    /** @override **/
    getTagName() {
        return "span";
    }
    /** @override **/
    getClassName() {
        return "";
    }
    /** @override **/
    getStyle() {
    }
   
    /** @override **/
    getName(){
        return "SpanClass"
    }
}
