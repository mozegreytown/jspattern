import {ContainerHandler} from "./";

export class ListClass extends ContainerHandler {
    constructor(props) {
        super(props)
    }
    /** @override **/
    getTagName() {
        return "ul";
    }
    /** @override **/
    getClassName() {
        return "";
    }
    /** @override **/
    getStyle() {
    }

   
    /** @override **/
    getName(){
        return "ListClass"
    }
}
