import {ContainerHandler} from "./";

export class CanvasClass extends ContainerHandler {
    constructor(props) {
        super(props)
    }

    /** @override **/
    getTagName() {
        return "canvas";
    }
    /** @override **/
    getClassName() {
        return "";
    }

    /** @override **/
    getStyle() {
    }
    /** @override **/
    getName(){
        return "CanvasClass"
    }

    saveCurrent(value) {
    }
}
