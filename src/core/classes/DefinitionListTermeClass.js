import {ContainerHandler} from "./";

export class DefinitionListTermeClass extends ContainerHandler {
    constructor(props) {
        super(props)
    }

    /** @override **/
    getTagName() {
        return "dt";
    }
    /** @override **/
    getClassName() {
        return "";
    }

    /** @override **/
    getStyle() {
    }

    /** @override **/
    getName(){
        return "DefinitionListTermeClass"
    }

    saveCurrent(value) {
    }
}
