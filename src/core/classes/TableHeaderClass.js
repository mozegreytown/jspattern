import {Parameters} from "./";

export class TableHeaderClass extends Parameters {
    constructor(props) {
        super(props)
    }
    /** @override **/
    getTagName() {
        return "thead";
    }
    /** @override **/
    getClassName() {
        return "";
    }
    /** @override **/
    getStyle() {
    }
    /** @override **/
    setObjectList() {
    }

    /** @override **/
    getName(){
        return "TableHeaderClass"
    }

}
