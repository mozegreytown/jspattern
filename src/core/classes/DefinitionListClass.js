import {ContainerHandler} from "./";

export class DefinitionListClass extends ContainerHandler {
    constructor(props) {
        super(props)
    }

    /** @override **/
    getTagName() {
        return "dl";
    }
    /** @override **/
    getClassName() {
        return "";
    }
    /** @override **/
    getStyle() {
    }

    /** @override **/
    getName(){
        return "DefinitionListClass"
    }

    saveCurrent(value) {
    }
}
