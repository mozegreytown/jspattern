import {Parameters} from "./";

export class TableHeadClass extends Parameters {
    constructor(props) {
        super(props)
    }
    /** @override **/
    getTagName() {
        return "th";
    }
    /** @override **/
    getClassName() {
        return "";
    }
    /** @override **/
    getStyle() {
    }
    /** @override **/
    setObjectList() {
    }

    /** @override **/
    getName(){
        return "TableHeadClass"
    }
}
