import {Parameters} from "./";

export class TableRowClass extends Parameters {
    constructor(props) {
        super(props)
    }
    /** @override **/
    getTagName() {
        return "tr";
    }
    /** @override **/
    getClassName() {
        return "";
    }
    /** @override **/
    getStyle() {
    }

    /** @override **/
    setObjectList() {
    }

    /** @override **/
    getName(){
        return "TableRowClass"
    }


}
