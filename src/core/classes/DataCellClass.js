import {Parameters} from "./";

export class DataCellClass extends Parameters {
    constructor(props) {
        super(props)
    }

    /** @override **/
    getTagName() {
        return "td";
    }
    /** @override **/
    getClassName() {
        return "";
    }
    /** @override **/
    getStyle() {
    }

    // TODO remove this pattern
    /** @override **/
    setObjectList() {
    }

    /** @override **/
    getName(){
        return "DataCellClass"
    }

    saveCurrent(value) {
    }
}
