import {ContainerHandler} from "./";

export class FigureClass extends ContainerHandler {
    constructor(props) {
        super(props)
    }

    /** @override **/
    getTagName() {
        return "figure";
    }
    /** @override **/
    getClassName() {
        return "";
    }
    /** @override **/
    getStyle() {
    }

    /** @override **/
    getName(){
        return "FigureClass"
    }
}
