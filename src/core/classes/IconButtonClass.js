import {ContainerHandler} from "./";

/** @abstract */
export class IconHandlerInterface extends ContainerHandler {
    constructor(props) {
        super(props);
    }
    /** @abstract */
    setIcon(props){}
}

export class IconButtonClass extends IconHandlerInterface {
    constructor(props) {
        super(props);
        this.setIcon(props);
    }

    setIcon(props) {
        if (props.icon && this.element) this.element.appendChild(props.icon);
    }

    /** @override **/
    getTagName() {
        return "button";
    }
    /** @override **/
    getClassName() {
        return "";
    }

    getStyle() {
    }

   
    /** @override **/
    getName(){
        return "IconButtonClass"
    }
}
