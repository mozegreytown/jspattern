import {isHome, pageName, pageTitle} from "../../utils/helpers";
import {Main} from "../../components";

export class Navigator {
    constructor(option) {
        this.navigate(option);
    }

    // USE MEMENTO PATTERN FOR NAV BACK AND NAV FORWARD

    navigate(option){
        history.pushState(null, option.title, isHome ? "/" : option.url);
    }

    goToUrl(option, domElementId = null){
        // fix this to reduce number of params to 3
        const {url, title, nextPage, stateObserver} = option || {};
        if (!url || !nextPage) return;
        document.title = title;
        const doc = document.getElementById(domElementId || "root");
        const page = nextPage(stateObserver);

        if (page && doc) {
            page.id = doc.id;
            const parent = Main({attributes: {id: doc.id}});
            parent.append(page);
            doc.replaceWith(parent);
            history.pushState({}, title, url);
        }
    }

    start(option, pageObjectKey = null) {
        option = option || {};
        const title = option.title || pageTitle || "404";
        return {value: option[pageObjectKey] || option[pageName] , title: title };
    }
}
