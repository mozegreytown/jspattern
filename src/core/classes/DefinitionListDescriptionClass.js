import {ContainerHandler} from "./";

export class DefinitionListDescriptionClass extends ContainerHandler {
    constructor(props) {
        super(props)
    }

    /** @override **/
    getTagName() {
        return "dd";
    }
    /** @override **/
    getClassName() {
        return "";
    }

    /** @override **/
    getStyle() {
    }

    /** @override **/
    getName(){
        return "DefinitionListDescriptionClass"
    }

    saveCurrent(value) {
    }
}
