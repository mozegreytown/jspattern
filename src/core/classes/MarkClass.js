import {ContainerHandler} from "./";

export class MarkClass extends ContainerHandler {
    constructor(props) {
        super(props)
    }

    /** @override **/
    getTagName() {
        return "mark";
    }
    /** @override **/
    getClassName() {
        return "";
    }
    /** @override **/
    getStyle() {
    }
   
    /** @override **/
    getName(){
        return "MarkClass"
    }
}
