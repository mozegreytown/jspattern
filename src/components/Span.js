import {SpanClass} from "../core/classes";

export function Span(props) {
    props = props || {};
    return new SpanClass(props).getValue();
}
