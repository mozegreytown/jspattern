import {FigureClass} from "../core/classes";

export function Figure(props) {
    props = props || {};
    return new FigureClass(props).getValue();
}
