import {IconClass} from "../core/classes";

export function Icon(props) {
    props = props || {};
    return new IconClass(props).getValue();
}
