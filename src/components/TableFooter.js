import {TableFooterClass} from "../core/classes";

export function TableFooter(props) {
    props = props || {};
    return new TableFooterClass(props).getValue();
}

