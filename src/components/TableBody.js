import {TableBodyClass} from "../core/classes";

export function TableBody(props) {
    props = props || {};
    return new TableBodyClass(props).getValue();
}

