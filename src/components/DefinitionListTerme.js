import {DefinitionListTermeClass} from "../core/classes";

export function DefinitionListTerme(props) {
    props = props || {};
    return new DefinitionListTermeClass(props).getValue();
}
