import {DefinitionListClass} from "../core/classes";

export function DefinitionList(props) {
    props = props || {};
    return new DefinitionListClass(props).getValue();
}
