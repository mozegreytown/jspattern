import {MarkClass} from "../core/classes";

export function Mark(props) {
    props = props || {};
    return new MarkClass(props).getValue();
}
