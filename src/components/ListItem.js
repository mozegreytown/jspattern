import {ListItemClass} from "../core/classes";

export function ListItem(props) {
  props = props || {};
  return new ListItemClass(props).getValue();
}
