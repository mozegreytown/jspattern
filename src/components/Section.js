import {SectionClass} from "../core/classes";
export function Section(props) {
    props = props || {};
    const section = new SectionClass(props);
    return section.getValue();
}
