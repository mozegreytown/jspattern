import {HeaderClass} from "../core/classes";

export function Header(props) {
    props = props || {};
    return new HeaderClass(props).getValue();
}
