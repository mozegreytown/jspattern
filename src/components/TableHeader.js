import {TableHeaderClass} from "../core/classes";

export function TableHeader(props) {
    props = props || {};
    return new TableHeaderClass(props).getValue();
}

