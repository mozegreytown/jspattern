import {GridClass} from "../core/classes";

export function Grid(props) {
    props = props || {};
    return new GridClass(props).getValue();
}
