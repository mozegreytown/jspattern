import {IconButtonClass} from "../core/classes";
import {Grid} from "./Grid";
import {Section} from "./Section";
import {Icon} from "./Icon";

export function IconButton(props) {
    props = props || {};
    props.attributes = props.attributes || {};
    const text = props.textContent || props.text || props.tc || props.title;
    if (props.textContent && props.icon || props.children && props.children.length > 0) {
        props.children = [
            Grid({
                gtc: props.gtc || "1fr auto",
                ai: props.ai || "center",
                children: !props.children ? [
                    typeof props.icon === "string" ? Section({
                        children: [
                            Icon({
                                textContent : props.icon,
                                attributes: props.attributes.icon
                            })
                        ]
                    }) :  props.icon,

                    typeof typeof text === "string" ?  Section({
                        textContent: props.textContent,
                        attributes: props.attributes.text
                    }) : props.textContent,
                ] : props.children
            })
        ];

        props.textContent = null;
        props.icon = null;
    }
    return new IconButtonClass(props).getValue();
}
