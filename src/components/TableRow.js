import {TableRowClass} from "../core/classes";

export function TableRow(props) {
    props = props || {};
    return new TableRowClass(props).getValue();
}

