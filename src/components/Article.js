import {ArticleClass} from "../core/classes";

export function Article(props) {
    props = props || {};
    return new ArticleClass(props).getValue();
}
