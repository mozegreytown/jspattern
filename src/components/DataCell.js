import {DataCellClass} from "../core/classes";

export function DataCell(props) {
    props = props || {};
    return new DataCellClass(props).getValue();
}

