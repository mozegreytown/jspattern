import {ListClass} from "../core/classes";

export function List(props) {
  props = props || {};
  return new ListClass(props).getValue();
}
