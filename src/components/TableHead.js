import {TableHeadClass} from "../core/classes";

export function TableHead(props) {
    props = props || {};
    return new TableHeadClass(props).getValue();
}

