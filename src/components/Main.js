import {MainClass} from "../core/classes";

export function Main(props) {
    props = props || {};
    return new MainClass(props).getValue();
}
