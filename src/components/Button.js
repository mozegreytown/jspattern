import {ButtonClass} from "../core/classes";

export function Button(props) {
    props = props || {};
    return new ButtonClass(props).getValue();
}
