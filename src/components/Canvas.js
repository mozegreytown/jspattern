import {CanvasClass} from "../core/classes";

export function Canvas(props) {
    props = props || {};
    return new CanvasClass(props).getValue();
}
