import {ParagraphClass} from "../core/classes";

export function Paragraph(props) {
    props = props || {};
    return new ParagraphClass(props).getValue();
}
