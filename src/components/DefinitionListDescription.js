import {DefinitionListDescriptionClass} from "../core/classes";

export function DefinitionListDescription(props) {
    props = props || {};
    return new DefinitionListDescriptionClass(props).getValue();
}
