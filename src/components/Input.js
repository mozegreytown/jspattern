import {InputClass} from "../core/classes";

export function Input(props) {
  props = props || {};
  return new InputClass(props).getValue();
}
