import {ImageClass} from "../core/classes";

export function Image(props) {
    props = props || {};
    return new ImageClass(props).getValue();
}
