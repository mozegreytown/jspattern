import {TableClass} from "../core/classes";

export function Table(props) {
    props = props || {};
    return new TableClass(props).getValue();
}

